#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import { TestseqofficialStack } from '../lib/testseqofficial-stack';

const app = new cdk.App();
new TestseqofficialStack(app, 'TestseqofficialStack');
